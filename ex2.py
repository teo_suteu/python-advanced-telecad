import re

input_string = input()

matched_string = re.findall("(r[a-zA-Z]+nt)", input_string)

print("original input: ", input_string)
print("matched input", matched_string)
