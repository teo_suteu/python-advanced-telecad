import os

# print(os.getcwd())
# print(os.listdir('..\PythonAdvanced1'))

def create_folder(path_file):
    if not os.path.exists(os.getcwd() + path_file):
        os.makedirs(os.getcwd() + path_file)


create_folder('/1/2/test2')
create_folder('/1/2/5/test5')
create_folder('/1/2/6/test6')
create_folder('/1/3/test3')
create_folder('/1/4/test4')


def write_in_file(path_file, string_to_write):
    with open(os.getcwd() + path_file, mode='wt') as file:
        for line in range(3):
            file.writelines(string_to_write + '\n')


write_in_file('/1/2/test2/Test2.txt', 'Test2')
write_in_file('/1/4/test4/Test4.txt', 'Test4')
write_in_file('/1/2/5/test5/Test5.txt', 'Test5')
write_in_file('/1/2/6/test6/Test6.txt', 'Test6')


with open(os.getcwd() + '/1/3/test3/Test3.txt', mode='wt') as file:
    for i in range(5):
        if i == 3:
            file.writelines('Stop' + '\n')
        else:
            file.writelines('Test3' + '\n')


def create_dictionary(text_string, file_path):
    my_dictionary = {}
    my_list = []
    for line in open(os.getcwd() + file_path, 'r').readlines():
        # print(line)
        my_list.append(line.strip())
    my_dictionary[file_path] = my_list
    print(text_string, my_dictionary)


create_dictionary('test2: ', '/1/2/test2/Test2.txt')
create_dictionary('test3: ', '/1/3/test3/Test3.txt')
create_dictionary('test4: ', '/1/4/test4/Test4.txt')
create_dictionary('test5: ', '/1/2/5/test5/Test5.txt')
create_dictionary('test6: ', '/1/2/6/test6/Test6.txt')


def create_custom_dictionary(text_string, file_path):
    my_dictionary = {}
    my_list = []
    found = False
    with open(os.getcwd() + file_path) as f:
        if 'Stop' in f.read():
            for line in open(os.getcwd() + file_path, 'r').readlines():
                # print(line)
                my_list.append(line.strip())
            my_dictionary[file_path] = my_list
            print(text_string, my_dictionary)
        else:
            print('no custom keyword found. nothing to append.')


create_custom_dictionary('custom test2: ', '/1/2/test2/Test2.txt')
create_custom_dictionary('custom test3: ', '/1/3/test3/Test3.txt')
